﻿using System;
using System.IO;
using TouchPortal.NET.Attributes;
using static TouchPortal.NET.Enums;
using Action = TouchPortal.NET.Attributes.Action;

namespace PluginExample
{
    [TouchPortal.NET.Attributes.Plugin(1, "#992211", "#0033d3", Name = "Example plugin entry file")
        , Category("exampleCategoryId", "Example Category", ImagePath = "%TP_PLUGIN_FOLDER%Example\\test.png")]
    class Plugin
    {
        [State("exampleCategoryId"
            , Id = "fruit"
            , DefaultValue = "Apple"
            , Description = "Fruit Kind description"
            , ValueChoices = new string[] { "Apple", "Pears", "Grapes", "Bananas" })
            , Event("When the chosen fruit is"
                , Id = "fruitEvent"
                , Name = "When the fruit is selected")]
        public string event001 = "Apple";

        [Action("exampleCategoryId"
            , Prefix = "Do the following action: "
            , Type = ActionType.Execute)]
        public static void FirstAction()
        {
            Console.WriteLine("First Action");
        }

        [Action("exampleCategoryId"
            , Prefix = "Do the following action: "
            , Type = ActionType.Execute)]
        public static void SecondAction(
              [Data(DefaultValue = "200", Label = "Number", ValueChoices = new string[] { "200", "400", "600", "800" })] int first
            , [Data(DefaultValue = "#ffaa33ff", Label = "Color")] string second
            , [Data(DefaultValue = "200", Label = "Number decimal", AllowDecimals = true)] double third
            , [Data(DefaultValue = "200", Label = "Text")] string fourth
            , [Data(DefaultValue = "200", Label = "File")] FileInfo fifth
            , [Data(DefaultValue = "200", Label = "Folder")] DirectoryInfo sixth
            , [Data(DefaultValue = "200", Label = "Choice")] string[] seventh
            , [Data(DefaultValue = "200", Label = "Switch")] bool eigth
        )
        {
            Console.WriteLine(first);
            Console.WriteLine(second);
        }
    }
}
