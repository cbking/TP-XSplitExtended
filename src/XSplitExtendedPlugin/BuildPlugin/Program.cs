﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using TouchPortal.NET.Attributes;

namespace BuildPlugin
{
    class Program
    {
        static void Main(string[] args)
        {
            var assembly = Assembly.Load(args[0]);
            
            var q = assembly.GetTypes().ToList();

            // Get all classes with the TouchPortalCategory
            var pluginClass = q.Where(t => t.CustomAttributes.Any(att => att.AttributeType == typeof(Plugin))).First();
            var plugin = (Plugin)pluginClass.GetCustomAttributes().First();
            var att = (Category[])Attribute.GetCustomAttributes(pluginClass, typeof(Category));
            plugin.Categories = att.ToList();
            plugin.Id = pluginClass.Name;

            var actions = pluginClass.GetMethods().Where(t => t.CustomAttributes.Any(att => att.AttributeType == typeof(TouchPortal.NET.Attributes.Action))).ToList();
            actions.ForEach(act =>
            {
                var actionAttribute = (TouchPortal.NET.Attributes.Action)Attribute.GetCustomAttribute(act, typeof(TouchPortal.NET.Attributes.Action));
                if (actionAttribute.Id is null)
                {
                    actionAttribute.Id = act.Name;
                }

                if (actionAttribute.Name is null)
                {
                    actionAttribute.Name = act.Name;
                }

                var parameters = act.GetParameters().Where(_ => _.CustomAttributes.Any(_ => _.AttributeType == typeof(Data))).ToList();

                if (parameters is null || parameters == new List<ParameterInfo>())
                {
                    //No Data Parameters
                }
                else
                {
                    parameters.ForEach(parm =>
                    {
                        var parmAttribute = (Data)Attribute.GetCustomAttribute(parm, typeof(Data));

                        if (actionAttribute.Data is null)
                        {
                            actionAttribute.Data = new List<Data>();
                        }

                        if (parmAttribute.Id is null)
                        {
                            parmAttribute.Id = parm.Name;
                        }

                        Regex rx = new Regex(@"#[a-fA-F0-9]{8}", RegexOptions.IgnoreCase);

                        switch (parm.ParameterType.Name)
                        {
                            case "String":
                                if (rx.IsMatch(parmAttribute.DefaultValue))
                                    parmAttribute.Type = "color";
                                else
                                    parmAttribute.Type = "text";
                                break;
                            case "Double":
                            case "Int32":
                                parmAttribute.Type = "number";
                                break;
                            case "Boolean":
                                parmAttribute.Type = "switch";
                                break;
                            case "String[]":
                                parmAttribute.Type = "choice";
                                break;
                            case "FileInfo":
                                parmAttribute.Type = "file";
                                break;
                            case "DirectoryInfo":
                                parmAttribute.Type = "folder";
                                break;
                            default:
                                parmAttribute.Type = "text";
                                break;
                        }
                        actionAttribute.Data.Add(parmAttribute);
                    });
                }

                plugin.Categories.Where(_ => _.Id == actionAttribute.CategoryId).First().Actions.Add(actionAttribute);
            });

            var states = pluginClass.GetMembers().Where(t => t.CustomAttributes.Any(att => att.AttributeType == typeof(State))).ToList();
            states.ForEach(state =>
            {
                var stateAttribute = (State)Attribute.GetCustomAttribute(state, typeof(State));

                if (stateAttribute.Id is null)
                {
                    stateAttribute.Id = state.Name;
                }

                if (stateAttribute.Description is null)
                {
                    stateAttribute.Description = state.Name;
                }

                plugin.Categories.Where(_ => _.Id == stateAttribute.CategoryId).First().States.Add(stateAttribute);

                if (state.CustomAttributes.Any(_ => _.AttributeType == typeof(Event)))
                {
                    var eventAttribute = (Event)Attribute.GetCustomAttribute(state, typeof(Event));

                    if (eventAttribute.Id is null)
                    {
                        stateAttribute.Id = state.Name;
                    }

                    eventAttribute.ValueStateId = stateAttribute.Id;
                    plugin.Categories.Where(_ => _.States.Any(_ => _.Id == eventAttribute.ValueStateId)).First().Events.Add(eventAttribute);
                }
            });

            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
                Converters = { new JsonStringEnumConverter( JsonNamingPolicy.CamelCase) }
            };  

            string path = @"Entry.json";
            string content = JsonSerializer.Serialize(plugin, options);
            using StreamWriter outFile = new StreamWriter(path);
            outFile.WriteLine(content);
        }
    }
}
