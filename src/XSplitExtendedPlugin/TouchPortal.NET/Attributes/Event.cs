﻿using System;
using System.Text.Json.Serialization;
using static TouchPortal.NET.Enums;

namespace TouchPortal.NET.Attributes
{
    /// <summary>
    /// Must be used with the <see cref="State"/> attribute.
    /// <seealso href="https://www.touch-portal.com/sdk/index.php?section=events">Touch Portal Documentation: Events</seealso>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class Event : Attribute
    {
        [JsonIgnore]
        public override object TypeId { get; }

        /// <summary>
        /// This is the id of the event. When the event is triggered, Touch Portal will send this information to the plugin with this id.
        /// Default is Field Name<para />
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// This is the name in the action category list.
        /// Default is Field Name<para />
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>
        /// This is the text the action will show in the user generated action list. The $val location will be changed with a dropdown holding the choices that the user can make for the status.
        /// Format should be something like `When my Custom State becomes $val`<para />
        /// </summary>
        [JsonPropertyName("format")]
        public string Format { get; }

        /// <summary>
        /// These are all the options the user can select in the event.
        /// Default is an empty string array<para />
        /// </summary>
        [JsonPropertyName("valueChoices")]
        public string[] ValueChoices { get; set; } = Array.Empty<string>();

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("type")]
        public EventType Type { get; } = EventType.Communicate;

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("valueStateId")]
        public string ValueStateId { get; set; }

        [JsonPropertyName("valueType")]
        public string ValueType { get; } = "choice";

        /// <param name="_format">Event format Format should be something like `When my Custom State becomes $val`<para /></param>
        public Event(string _format)
        {
            Format = _format;
        }
    }
}
