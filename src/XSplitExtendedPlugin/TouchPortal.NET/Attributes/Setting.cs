﻿using System;
using System.Text.Json.Serialization;

namespace TouchPortal.NET.Attributes
{
    /// <summary>
    /// <see href="https://www.touch-portal.com/sdk/index.php?section=settings">TP Documentation: Settings</see>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class Setting : Attribute
    {
        [JsonIgnore]
        public override object TypeId { get; }

        /// <summary>
        /// Setting name <para />
        /// Default is Field name
        /// </summary>
        public string name { get; set; } = string.Empty;

        /// <summary>
        /// Setting defaultValue
        /// </summary>
        public string defaultValue { get; set; } = string.Empty;

        /// <summary>
        /// Setting maximum length <para />
        /// Used if the setting type is text
        /// </summary>
        public double maxLength { get; set; } = 0;

        /// <summary>
        /// Setting isPassword <para />
        /// Used if the parameter type is text
        /// </summary>
        public bool isPassword { get; set; } = false;

        /// <summary>
        /// Setting minimum value <para />
        ///  Used if the parameter type is number
        /// </summary>
        public double minValue { get; set; } = double.NegativeInfinity;

        /// <summary>
        /// Setting maximum value <para />
        ///  Used if the parameter type is number
        /// </summary>
        public double maxValue { get; set; } = double.PositiveInfinity;

        /// <summary>
        /// Setting readOnly
        /// </summary>
        public bool isReadOnly { get; set; } = false;
    }
}
