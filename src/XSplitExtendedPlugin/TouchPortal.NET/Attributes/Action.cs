﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using static TouchPortal.NET.Enums;

namespace TouchPortal.NET.Attributes
{
    ///<summary>
    /// <b>If <see cref="Data"/> is the only parameter attribute, the method will be called automatically by the SDK.</b><para />
    /// <i>If it is not the case, the SDK will call the TouchPortalPluginListener.onReceive(JsonObject jsonMessage) instead.</i><para />
    /// <seealso href="https://www.touch-portal.com/sdk/index.php?section=dynamic-actions">Touch Portal Documentation: Dynamic Actions</seealso>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class Action : Attribute
    {
        [JsonIgnore]
        public override object TypeId { get; }

        ///<summary>
        /// This is the id of the action.
        /// It is used to identify the action within Touch Portal. 
        /// This id needs to be unique across plugins. 
        /// This means that if you give it the id "1" there is a big chance that it will be a duplicate. 
        /// Touch Portal may reject it or when the other action is called, yours will be as well with wrong data. 
        /// Best practise is to create a unique prefix for all your actions. <para />
        /// Default is the method name.
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        ///<summary>
        /// This is the name of the action. This will be used as the action name in the action category list.<para />
        /// Default is the method name.
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        ///<summary>
        /// This is the text that is displayed before the name of the action in the action list of a button.<para />
        /// Default is "Plugin" 
        /// </summary>
        [JsonPropertyName("prefix")]
        public string Prefix { get; set; } = "Plugin";

        ///<summary>
        /// This will add text to the popup window. It will be shown on the top of the popup. This can be used to explain parts of the plugin data.<para/>
        /// From version 2.0 of the Api (Touch Portal v2.2) this description will also be used in the inline actions on top to be able to add information about the action where applicable.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("description")]
        public string Description { get; set; }

        ///<summary>
        /// This is the attribute that specifies whether this is a static action (<see cref="ActionType.Exectue"/>) or a dynamic action (<see cref="ActionType.Communicate"/>). <para />
        /// Default is "communicate".
        /// </summary>
        [JsonPropertyName("type")]
        public ActionType Type { get; set; } = ActionType.Communicate;

        ///<summary>
        /// This will be the format of the inline action. Use the id's of the data objects to place them within the text. <para />
        ///    Example:
        ///    "format":"When {$actiondata001$} has {$actiondata002$} and number {$actiondata003$} is {$actiondata004$}",<para />
        /// With this example, the data object with the id 'actiondata001' will be shown at the given location. 
        /// To have an data object appear on the action line, use the format {$id$} where id is the id of the data object you want to show the control for.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("format")]
        public string Format { get; set; }

        ///<summary>
        /// Action category Id.
        /// </summary>
        [JsonPropertyName("categoryId")]
        public string CategoryId { get; }

        ///<summary>
        /// <b>This switch will only work with dynamic actions.</b> <para/>
        /// Adding this attribute to the action will allow the action to be used with the hold functionality, allowing it to appear in the hold action list.<para />
        /// Options are "true" or "false". Default is false. 
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("hasHoldFunctionality")]
        public bool HasHoldFunctionality { get; set; } = false;

        /// <summary>
        /// If true, user can edit data from the action list instead of a popup.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("tryInline")]
        public bool TryInline { get; set; }

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("data")]
        public List<Data> Data { get; set; }

        public Action(string _categoryId)
        {
            CategoryId = _categoryId;
        }
    }
}
