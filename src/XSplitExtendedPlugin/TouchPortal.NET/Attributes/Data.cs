﻿using System;
using System.Text.Json.Serialization;

namespace TouchPortal.NET.Attributes
{
    ///<summary>
    /// The paramater type determines the type Touch Point will use: <para/>
    /// text: A data type that accepts a string. Use <see cref="string"/><para/>
    /// number: A data type that accepts a number. Use <see cref="int"/> or <see cref="double"/><para/>
    /// switch:	A data type that accepts a true or false value. Use <see cref="bool"/> <para/>
    /// choice: A data type that accepts a string where a collection of strings can be chosen from. Use <see cref="string"/>[]<para/>
    /// file: A data type that represents a file which the user can pick with a file chooser. Use <see cref="System.IO.FileInfo"/><para/>
    /// folder: A data type that represents a folder which the user can pick with a folder chooser. Use <see cref="System.IO.DirectoryInfo"/><para/>
    /// color: A data type that represents a color which the user can pick with a color chooser. Use <see cref="string"/>. This value must be in a the format #RRGGBBAA.<para />
    /// <seealso href="https://www.touch-portal.com/sdk/index.php?section=action-data">Touch Portal Documentation: Action Data</seealso>
    ///</summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class Data : Attribute
    {
        [JsonIgnore]
        public override object TypeId { get; }

        ///<summary>
        /// This is the id of the data field. Touch Portal will use this for communicating the values or to place the values in the result. <para />
        /// Default is the parameter name.
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        ///<summary>
        /// This is the text used in the popup windows. Default is the parameter name.
        /// </summary>
        [JsonPropertyName("label")]
        public string Label { get; set; }

        ///<summary>
        /// This is the default value the data object has. Use the correct types of data for the correct type of data object. Empty string by default.
        /// Enter '0' for a number<para />
        /// Enter 'false' for a switch<para />
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("default")]
        public string DefaultValue { get; set; }

        ///<summary>
        /// This is a collection of strings that the user can choose from.. Default is an empty string array.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("valueChoices")]
        public string[] ValueChoices { get; set; }

        ///<summary>
        /// This is a collection of extensions allowed to open. <i>This only has effect when used with the file type.</i>
        /// Use wildcard to filter supported extensions (e.g. *.txt) <para />
        /// Default value is an empty list meaning all file types are supported.<para />
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("extensions")]
        public string[] Extensions { get; set; }

        ///<summary>
        /// This is the lowest number that will be accepted. The user will get a message to correct the data if it is lower and the new value will be rejected.
        /// Used if the parameter type is number<para />
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("minValue")]
        public double MinimumValue { get; set; }

        /// <summary>
        /// This is the highest number that will be accepted. The user will get a message to correct the data if it is higher and the new value will be rejected.
        /// Used if the parameter type is number.<para />
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("maxValue")]
        public double MaximumValue { get; set; }

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("type")]
        public string Type { get; set; } = string.Empty;

        /// <summary>
        /// This field can only be used with the "number" type and tells the system whether this data field should allow decimals in the number. The default is "true".
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("allowDecimals")]
        public bool AllowDecimals { get; set; }
    }
}
