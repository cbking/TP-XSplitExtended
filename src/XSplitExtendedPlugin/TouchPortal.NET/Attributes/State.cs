﻿using System;
using System.Text.Json.Serialization;

namespace TouchPortal.NET.Attributes
{
    /// <summary>
    /// <seealso href="https://www.touch-portal.com/sdk/index.php?section=states">Touch Portal Documentation: States</seealso><para />
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class State : Attribute
    {
        [JsonIgnore]
        public override object TypeId { get; }
        /// <summary>
        /// This is the id of the state. 
        /// It is used to identify the states within Touch Portal. This id needs to be unique across plugins. 
        /// This means that if you give it the id "1" there is a big chance that it will be a duplicate. 
        /// Touch Portal may reject it or when the other state is updated, yours will be as well with wrong data. 
        /// Best practise is to create a unique prefix for all your states like in our case; tp_sid_fruit. 
        /// Defaults to field name.
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// This text describes the state and is used in the IF statement to let the user see what state it is changing. 
        /// We recommend to make this text work in the flow of the inline nature of the IF statement within Touch Portal. 
        /// This is also the title that is used in list where you can use this state value for logic execution. 
        /// Defaults to field name.
        /// </summary>
        [JsonPropertyName("desc")]
        public string Description { get; set; } 

        /// <summary>
        /// This is the value the state will have if it is not set already but is looked up.
        /// </summary>
        [JsonPropertyName("default")]
        public string DefaultValue { get; set; }

        /// <summary>
        /// Specify the collection of values that can be used to choose from. 
        /// These can also be dynamically changed if you use the dynamic actions.
        /// Default is an empty string array.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("valueChoices")]
        public string[] ValueChoices { get; set; }

        /// <summary>
        /// State category identifier.
        /// </summary>
        [JsonPropertyName("categoryId")]
        public string CategoryId { get; }

        [JsonPropertyName("type")]
        public string ValueType { get; } = "choice";

        /// <param name="_categoryId"/>Category Identifier</param>
        public State(string _categoryId)
        {
            CategoryId = _categoryId;
        }
    }
}
