﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TouchPortal.NET.Attributes
{
    ///<summary>
    ///<see href="https://www.touch-portal.com/sdk/index.php?section=categories">Touch Portal Documentation: Categories</see>
    ///</summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class Category : Attribute
    {
        [JsonIgnore]
        public override object TypeId { get; }

        /// <summary>
        /// This is the id of the category.
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; }

        /// <summary>
        /// This is the name of the category.
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; }

        /// <summary>
        /// This is the absolute path to an icon for the category. 
        /// You should place the image in your plugin folder and reference it. 
        /// If you use %TP_PLUGIN_FOLDER% in the text here, it will be replaced with the path to the base plugin folder.
        /// <para />
        /// Image specification<para />
        /// Format: 32bit PNG<para />
        /// Resolution: 24x24<para />
        /// Color: White with transparent background
        /// </summary>
        [JsonPropertyName("imagePath")]
        public string ImagePath { get; set; } = string.Empty;

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("actions")]
        public List<Action> Actions { get; set; } = new List<Action>();

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("events")]
        public List<Event> Events { get; set; } = new List<Event>();

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("states")]
        public List<State> States { get; set; } = new List<State>();

        public Category(string _id, string _name)
        {
            Id = _id;
            Name = _name;
            ImagePath = string.Empty;
        }
    }
}
