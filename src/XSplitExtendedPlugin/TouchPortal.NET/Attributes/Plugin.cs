﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TouchPortal.NET.Attributes
{
    /// <summary>
    /// <seealso href="https://www.touch-portal.com/sdk/index.php?section=structure">Touch Portal Documentation: Plugin Structure</seealso>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class Plugin : Attribute
    {
        [JsonIgnore]
        public override object TypeId { get; }

        public class Configuration
        {
            /// <summary>
            /// Plugin color light
            /// Format is RBG hex `#RRGGBB`<para />
            /// </summary>
            [JsonPropertyName("colorLight")]
            public string ColorLight { get; set; }

            /// <summary>
            /// Plugin colorDark
            /// Format RBG hex `#RRGGBB`<para />
            /// </summary>
            [JsonPropertyName("colorDark")]
            public string ColorDark { get; set; }
        }

        /// <summary>
        /// This is the name of the Plugin. This will show in Touch Portal in the settings section "Plug-ins". (From Touch Portal version 2.2)
        /// <para>Default is class name>/para>
        /// </summary>  
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// A number representing your own versioning. 
        /// Currently this variable is not used by Touch Portal but may be used in the future. 
        /// This should be an integer value. So only whole numbers, no decimals. 
        /// This version value will be send back after the pairing has been done.
        /// </summary>
        [JsonPropertyName("version")]
        public int Version { get; }

        /// <summary>
        /// Plugin colorDark
        /// Format RBG hex `#RRGGBB`<para />
        /// </summary>
        [JsonIgnore]
        public string ColorDark { get; }

        /// <summary>
        /// Plugin color light
        /// Format is RBG hex `#RRGGBB`<para />
        /// </summary>
        [JsonIgnore]
        public string ColorLight { get; }

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("sdk")]
        public string SDK { get; } = "2";

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; } = string.Empty;

        /// <summary>
        /// Specify the path of execution here. You should be aware that it will be passed to the OS process exection service. 
        /// This means you need to be aware of spaces and use absolute paths to your executable. <para />
        /// If you use %TP_PLUGIN_FOLDER% in the text here, it will be replaced with the path to the base plugins folder.
        /// So append your plugins folder name to it as well to access your plugin base folder. <para />
        /// This execution will be done when the plugin is loaded in the system and only if it is a valid plugin.
        /// Use this to start your own service that communicates with the Touch Portal plugin system.
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("plugin_start_cmd")]
        public string PluginStartCommand { get; set; }

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("categories")]
        public List<Category> Categories { get; set; } = new List<Category>();

        /// <summary>
        /// <b>Do not set this property. It won't be used.</b>
        /// </summary>
        [JsonPropertyName("configuration")]
        public Configuration Config
        {
            get
            {
                return new Configuration { ColorDark = ColorDark, ColorLight = ColorLight };
            }
        }

        /// <param name="_version">Plugin version</param>
        /// <param name="_colorDark">Plugin dark color. Format is RBG hex `#RRGGBB`</param>
        /// <param name="_colorLight">Plugin light color. Format is RBG hex `#RRGGBB`</param>
        public Plugin(int _version, string _colorDark, string _colorLight)
        {
            Version = _version;
            ColorDark = _colorDark;
            ColorLight = _colorLight;
        }
    }
}
